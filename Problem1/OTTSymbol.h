//
//  OTTSymbol.h
//  Problem1
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTTSymbol : NSObject

@property (strong, nonatomic) NSNumber *index;
@property (strong, nonatomic) NSString *character;
@property NSInteger row;
@property NSInteger column;

- (instancetype) initWithCharacter:(NSString *)character index:(NSNumber *)index row:(NSInteger)row column:(NSInteger)column;
+ (NSArray *)targetSymbols;

@end
