//
//  OTTFileReader.h
//  Problem1
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OTTInputOutput : NSObject

- (NSArray *)matrix;
- (void)printResult:(NSArray *)symbols;

@end
