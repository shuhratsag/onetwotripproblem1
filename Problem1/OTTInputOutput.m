//
//  OTTFileReader.m
//  Problem1
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

#import "OTTInputOutput.h"
#import "OTTSymbol.h"

@implementation OTTInputOutput

- (NSArray *)matrix {

    FILE *inputFile = fopen("input.txt", "r");
    if (inputFile) {
        NSInteger m, n;
        fscanf(inputFile, "%ld", &m);
        fscanf(inputFile, "%ld", &n);
        NSMutableArray *matrix = [@[] mutableCopy];
        char c;
        for (int i = 0; i < m; ++i) {
            NSMutableArray *row = [@[] mutableCopy];
            for (int j = 0; j < n; ++j) {
                fscanf(inputFile, " %c", &c);
                [row addObject:[NSString stringWithFormat:@"%c", c ]];
            }
            [matrix addObject:row];
        }
        return matrix;
    }
    return nil;
}

- (void)printResult:(NSArray *)symbols {
    if (symbols) {
        FILE *outputFile = fopen("output.txt", "w");
        for (OTTSymbol *symbol in symbols) {
            fprintf(outputFile, "%s - (%ld, %ld);\n", [symbol.character UTF8String], symbol.row, symbol.column);
        }
    } else {
        NSLog(@"Impossible");
    }
}

@end
