//
//  OTTSolver.h
//  Problem1
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTTSolver : NSObject
- (NSArray *)resultArrayForMatrix:(NSArray *)matrix;
@end
