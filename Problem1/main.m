//
//  main.m
//  Problem1
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OTTInputOutput.h"
#import "OTTSolver.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        OTTInputOutput *io = [OTTInputOutput new];
        NSArray *matrix = [io matrix];
        if (matrix) {
            NSArray *symbols = [[OTTSolver new] resultArrayForMatrix:matrix];
            [io printResult:symbols];
        } else
            NSLog(@"No Input File");
    }
    return 0;
}
