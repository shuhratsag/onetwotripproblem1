//
//  OTTSolver.m
//  Problem1
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

#import "OTTSolver.h"
#import "OTTSymbol.h"

@implementation OTTSolver

{
    NSMutableArray *targetSymbols;
    NSMutableArray *resultSymbols;
}

// Returns array of intended "SYMBOL"s or nil if solution is not found
- (NSArray *)resultArrayForMatrix:(NSArray *)matrix {
    for (NSArray *row in matrix) {
        for (NSString *character in row) {
            NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(OTTSymbol *symbol, NSDictionary *bindings) {
                return [symbol.character caseInsensitiveCompare:character] == NSOrderedSame;
            }];
            NSArray *filteredSymbols = [targetSymbols filteredArrayUsingPredicate:predicate];
            if (filteredSymbols.count > 0) {
                OTTSymbol *foundSymbol = [filteredSymbols firstObject];
                OTTSymbol *symbol = [[OTTSymbol alloc] initWithCharacter:character index:foundSymbol.index row:[matrix indexOfObject:row] column:[row indexOfObject:character]];
                [resultSymbols addObject:symbol];
                [targetSymbols removeObject: foundSymbol];
            }
        }
    }
    [resultSymbols sortUsingComparator:^NSComparisonResult(OTTSymbol *obj1, OTTSymbol *obj2) {
        if ([obj2.index isGreaterThanOrEqualTo:obj1.index])
            return (NSComparisonResult)NSOrderedAscending;
        else
            return (NSComparisonResult)NSOrderedDescending;
    }];
    return (targetSymbols.count == 0) ? resultSymbols : nil;
}

- (id)init {
    if (self = [super init]) {
        targetSymbols = [[OTTSymbol targetSymbols] mutableCopy];
        resultSymbols = [@[] mutableCopy];
    }
    return self;
}

@end
