//
//  OTTSymbol.m
//  Problem1
//
//  Created by admin on 04.10.16.
//  Copyright © 2016 OneTwoTrip. All rights reserved.
//

#import "OTTSymbol.h"

@implementation OTTSymbol

- (instancetype) initWithCharacter:(NSString *)character index:(NSNumber *)index {
    return [self initWithCharacter:character index:index row:0 column:0];
}
- (instancetype) initWithCharacter:(NSString *)character index:(NSNumber *)index row:(NSInteger)row column:(NSInteger)column {
    if (self = [super init]) {
        _character = character;
        _index = index;
        _row = row;
        _column = column;
    }
    return self;
}

+ (NSArray *)targetSymbols {
    return @[
                       [[self alloc] initWithCharacter:@"O" index:@0],
                       [[self alloc] initWithCharacter:@"n" index:@1],
                       [[self alloc] initWithCharacter:@"e" index:@2],
                       [[self alloc] initWithCharacter:@"T" index:@3],
                       [[self alloc] initWithCharacter:@"w" index:@4],
                       [[self alloc] initWithCharacter:@"o" index:@5],
                       [[self alloc] initWithCharacter:@"T" index:@6],
                       [[self alloc] initWithCharacter:@"r" index:@7],
                       [[self alloc] initWithCharacter:@"i" index:@8],
                       [[self alloc] initWithCharacter:@"p" index:@9],
                       ];
}
@end
